﻿using System;
using AssemblyCSharp.Scripts.Sands;
using System.Collections.Generic;
using UnityEngine;
using AssemblyCSharp.Scripts.Sands.Fungus;
using AssemblyCSharp.Scripts.Sands.Vine;
using AssemblyCSharp.Scripts.Sands.Roots;
using AssemblyCSharp.Scripts.Sands.BridgeFungus;
using AssemblyCSharp.Scripts.Sands.Emitters;
using AssemblyCSharp.Scripts.Sands.Meta;
using AssemblyCSharp.Scripts.Sands.Traps;
using AssemblyCSharp.Scripts.Sands.Vegetation;
using AssemblyCSharp.Scripts.Sands.Electrical;

namespace AssemblyCSharp.Scripts
{
    public static class SandMan
    {
        private static readonly Sand[] _sands = new Sand[]
        {
                new Nothing(),
                new Bedrock(),
                new Dirt(),
                new Grass(),
                new Rock(),
                new Player(),
                new PlayerDead(),
                new Water(),
                new Moss(),
                new Exit(),
                new Lava(),
                new GrowingFungus(),
                new AliveFungus(),
                new DeadGrowth(),
                new TransmittingFungus(),
                new TransmittingRoot(),
                new AliveRoot(),
                new Bricks(),
                new Writing(),
                new Vine(),
                new VineRoot(),
                new VineSearcher(),
                new VineActive(),
                new VineSearched(),
                new DeadVine(),
                new PlayerSleep(),
                new Fire(),
                new Wood(),
                new AliveBridgeFungus(),
                new GrowingBridgeFungus(),
                new TransmittingBridgeFungus(),
                new HangingBridgeFungus(),
                new LiquidAbsorber(),
                new LavaEmitter(),
                new BrickWallBuilder(),
                new WaterBucket(),
                new TrapWall(),
                new DisappearingWall(),
                new Foliage(),
                new TreeTrunk(),
                new TreeSeed(),
                new TreeFoliageGrowth(),
                new FlowerSeed(),
                new FlowerYellow(),
                new Wire(),
                new WireOn(),
                new TriggerSimulator(),
                new TriggerPlayer(),
                new ReactorSpark(),
                new PoweredDoor(),
                new PlayerEmitter(),
                new PlayerAbsorber(),
        };

        private static readonly Dictionary<Type, byte> _sandTypes = new Dictionary<Type, byte>();
        private static readonly Dictionary<string, byte> _sandNames = new Dictionary<string, byte>(StringComparer.OrdinalIgnoreCase);

        static SandMan()
        {
            for (int i=0; i<_sands.Length; ++i)
            {
                _sands[i].Id = (byte)i;
                _sandTypes[_sands[i].GetType()] = (byte)i;
                _sandNames[_sands[i].GetType().Name] = (byte)i;
            }
        }

        private static class FastLookup<T>
        {
            public static byte? Id;
        }

        public static byte GetId<T>()
            where T : Sand
        {
            if (FastLookup<T>.Id.HasValue)
                return FastLookup<T>.Id.Value;
            FastLookup<T>.Id = _sandTypes[typeof(T)];
            return FastLookup<T>.Id.Value;
        }

        public static Sand Get<T>()
            where T : Sand
        {
            return _sands[GetId<T>()];
        }

        public static T GetAs<T>()
            where T : Sand
        {
            return _sands[GetId<T>()] as T;
        }

        public static Sand GetById(byte id)
        {
            return _sands[id];
        }

        public static Sand GetByNearestColor(Color32 color)
        {
            Sand curr = Empty;
            int currDist = int.MaxValue;

            for (int i=0; i<_sands.Length; ++i)
            {
                var sandColor = _sands[i].Color;
                int dist = Math.Abs(sandColor.r - color.r) + Math.Abs(sandColor.g - color.g) + Math.Abs(sandColor.b - color.b);
                if (dist < currDist)
                {
                    curr = _sands[i];
                    currDist = dist;
                }
            }

            return curr;
        }

        public static Sand SlowGetByName(string name)
        {
            byte id;
            if (_sandNames.TryGetValue(name, out id))
            {
                return _sands[id];
            }
            return null;
        }

        public static Sand Empty
        {
            get{return _sands[0];}
        }

        public static Sand Bedrock
        {
            get{return _sands[1];}
        }
    }
}

