﻿using System;

namespace AssemblyCSharp.Scripts.Containers
{
    public class Buffer2d
    {
        private readonly byte[] _data;

        public readonly int Width, Height;

        public Buffer2d(int width, int height)
        {
            Width = width;
            Height = height;
            _data = new byte[width*height];
        }

        public byte this[int x, int y]
        {
            get{return _data[y * Width + x];}
            set{_data[y * Width + x] = value;}
        }

        public byte GetWrapped(int x, int y)
        {
            return _data[MathHelper.mmod(y, Height) * Width + MathHelper.mmod(x, Width)];
        }
    }
}

