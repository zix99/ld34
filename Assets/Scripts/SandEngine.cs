﻿using UnityEngine;
using AssemblyCSharp.Scripts.Jobs;
using AssemblyCSharp.Scripts;
using AssemblyCSharp.Scripts.Sands;
using UnityEngine.UI;
using AssemblyCSharp.Scripts.WorldGens;
using UnityEngine.EventSystems;
using AssemblyCSharp.Scripts.Items;
using System.Collections.Generic;

public class SandEngine : MonoBehaviour, IPointerClickHandler
{
    public const int WORLD_SIZE = 4096;

    private SandWorld _world; 
    private SandTextureRenderer _sandRenderer;
    private SandSimulator _simulator;

    private Level _currentLevel;
    private bool _levelComplete;

    private int _score = 0;

    public int StartLevel = -1;

    public ItemsBar ItemsBar;

    public Text DebugText;

    public MeshRenderer Background;

    public AudioSource SfxSource;
    public AudioSource SfxMovement;
    public AudioClip SfxLevelComplete;
    public AudioClip SfxLevelLost;

    void Start()
    {
        this.DebugText.text = string.Empty;

        Events.LevelCompleteEvent += LevelCompleted;

        var mesh = this.GetComponent<MeshRenderer>();

        _world = new SandWorld(WORLD_SIZE, WORLD_SIZE);
        _sandRenderer = new SandTextureRenderer(mesh, _world);
        _sandRenderer.Start();

        _simulator = new SandSimulator(_world);
        _simulator.Start();

        LoadLevel(this.StartLevel);
    }
	
    private void LoadLevel(int number)
    {
        this.ItemsBar.SetScore(_score);

        _currentLevel = new Level(number);
        _simulator.Clear();
        _simulator.Listen = false;

        SandMan.GetAs<Player>().ResetPlayers();
        _currentLevel.LoadWorld(_world);

        this.Background.material.mainTexture = _currentLevel.LoadBackground();

        //Load items
        ItemsBar.Clear();

        foreach(var item in _currentLevel.GetItemAndCount())
        {
            var sand = SandMan.SlowGetByName(item.Key);
            if (sand != null)
            {
                ItemsBar.AddItem(sand, item.Value);
            }
        }

        //Re-enable simulator before touching
        _simulator.Listen = true;

        //Touch player to make work
        foreach(var pt in SandMan.GetAs<Player>().LockGetPlayers())
        {
            _simulator.Touch(pt.X, pt.Y);
        }

        //Touch all immediately activated blocks
        for (int y=0; y<_world.Height; ++y)
        {
            for (int x=0; x<_world.Width; ++x)
            {
                if (_world[x, y].SimulateImmediately)
                    _simulator.Touch(x, y);
            }
        }

        _levelComplete = false;
    }

    private void LoadNextLevel()
    {
        SfxSource.PlayOneShot(this.SfxLevelComplete);
        LoadLevel(_currentLevel.LevelNumber+1);
    }

    private void ResetLevel()
    {
        int levelNum = -1;
        if (_currentLevel != null)
            levelNum = _currentLevel.LevelNumber;

        SfxSource.PlayOneShot(this.SfxLevelLost);
        LoadLevel(levelNum);
    }

    void Update()
    {
        var p = SandMan.Get<Player>() as Player;
        var center = p.GetCenter();
        _sandRenderer.Recenter(center.X, center.Y);

        if(_levelComplete)
        {
            _score += p.PlayerCount;
            LoadNextLevel();
        }

        ItemsBar.SetPower(p.PlayerCount);

        if (p.PlayerCount == 0 || Input.GetKeyDown(KeyCode.R))
        {
            _score -= 50;
            ResetLevel();
        }

        var player = SandMan.GetAs<Player>();
        player.Horizontal = Input.GetAxis("Horizontal");
        player.Vertical = Input.GetAxis("Vertical");
        _sandRenderer.Apply();

        this.PlayWorldSounds();

        if (Mathf.Abs(player.Horizontal) > 0.5f || Mathf.Abs(player.Vertical) > 0.5f)
        {
            this.PlayMovementSfx();
        }

        #if UNITY_WEBGL
        _simulator.SyncUpdate();
        _sandRenderer.SyncUpdate();
        #endif

        #if DEVELOPMENT_BUILD || DEBUG
        this.DebugText.text = string.Format(
            "Render Thread: {0:F2}/{1:F2}\nSim Thread: {2:F2}/{3:F2}\nActive: {4}/{5}",
            _sandRenderer.AvgFrameTime, _sandRenderer.MillisPerFrame,
            _simulator.AvgFrameTime, _simulator.MillisPerFrame,
            _simulator.ActiveCount, _simulator.ListCount
        );
        #endif
    }

    private Dictionary<string, float> _playingUntil = new Dictionary<string, float>();
    private void PlayWorldSounds()
    {
        foreach(var sound in _world.GetSounds())
        {
            if(!_playingUntil.ContainsKey(sound) || _playingUntil[sound] < Time.time)
            {
                var clip = Resources.Load<AudioClip>(sound);
                _playingUntil[sound] = Time.time + clip.length;
                AudioSource.PlayClipAtPoint(clip, new Vector3());
            }
        }
    }

    private Sand _currentPlayingMovement;
    private void PlayMovementSfx()
    {
        var player = SandMan.GetAs<Player>();

        Sand sandType = _world.ClosestWhere(player.Center.X, player.Center.Y, 20, p => !(p is Player) && !p.Empty);
        if (sandType != null && string.IsNullOrEmpty(sandType.SfxMovement))
            sandType = null;

        if (sandType != null && sandType.SfxMovement != null && sandType != _currentPlayingMovement)
        {
            Debug.Log("Starting to play " + sandType.SfxMovement);
            SfxMovement.clip = Resources.Load<AudioClip>(sandType.SfxMovement);
            SfxMovement.Play();
            _currentPlayingMovement = sandType;
        }
        else if (_currentPlayingMovement != null && sandType == null)
        {
            Debug.Log("Stopping");
            SfxMovement.Stop();
            _currentPlayingMovement = null;
        }
    }

    void OnApplicationQuit()
    {
        _sandRenderer.Stop();
        _simulator.Stop();
    }

    void OnApplicationPause(bool isPaused)
    {
        if (_sandRenderer == null || _simulator == null)
            return;

        if (isPaused)
        {
            Debug.Log("Pausing threads...");
            _sandRenderer.Pause();
            _simulator.Pause();
        }
        else
        {
            Debug.Log("Unpausing threads...");
            _sandRenderer.Resume();
            _simulator.Resume();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var ray = Camera.main.ScreenPointToRay(eventData.position);
        RaycastHit hit;
        if(Physics.Raycast(ray, out hit))
        {
            var p = _sandRenderer.UVToSandCoord(hit.textureCoord);
            float dist = ((Vector2)SandMan.GetAs<Player>().Center - p).magnitude;

            if (dist < Mathf.Sqrt(SandMan.GetAs<Player>().PlayerCount) * 3f)
            {
                if (eventData.button == PointerEventData.InputButton.Left && ItemsBar.Selected != null && ItemsBar.Selected.Quantity > 0)
                {
                    ItemsBar.Selected.Quantity--;
                    _world[p.X, p.Y] = ItemsBar.Selected.Sand;
                    SfxSource.PlayOneShot(Resources.Load<AudioClip>(ItemsBar.Selected.Sand.SfxPlace));
                }
            }
        }
    }

    public void LevelCompleted()
    {
        _levelComplete = true;
    }
}
