﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts
{
    public struct Point2i : IEquatable<Point2i>
    {
        public int X, Y;

        public Point2i(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static implicit operator Vector2(Point2i pt)
        {
            return new Vector2(pt.X, pt.Y);
        }

        public static Point2i operator-(Point2i left, Point2i right)
        {
            return new Point2i(left.X - right.X, left.Y - right.Y);
        }

        public double Length
        {
            get
            {
                return Math.Sqrt(X * X + Y * Y);
            }
        }

        #region IEquatable implementation

        public bool Equals(Point2i other)
        {
            return this.X == other.X && this.Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Point2i))
                return false;

            var other = (Point2i)obj;
            return other.X == this.X && other.Y == this.Y;
        }

        public override int GetHashCode()
        {
            return X ^ Y;
        }

        #endregion

        public static readonly Point2i Zero = new Point2i(0,0);

        public static readonly Point2i UnitX = new Point2i(1,0);

        public static readonly Point2i UnitY = new Point2i(0,1);

        public static readonly Point2i[] AxisSides = new Point2i[]{
            new Point2i(-1,0), new Point2i(1,0), new Point2i(0,-1), new Point2i(0,1)
        };

        public static readonly Point2i[] AllSides = new Point2i[]
        {
            new Point2i(-1, 0), new Point2i(1, 0), new Point2i(0, -1), new Point2i(0, 1),
            new Point2i(-1, -1), new Point2i(-1, 1), new Point2i(1, -1), new Point2i(1, 1)
        };

        public static readonly Point2i[] XAxis = new Point2i[]
        {
                new Point2i(-1,0), new Point2i(1,0)
        };
    }
}

