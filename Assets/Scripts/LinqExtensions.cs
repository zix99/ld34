﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AssemblyCSharp.Scripts
{
    public static class LinqExtensions
    {
        private static readonly System.Random _random = new System.Random();

        public static IEnumerable<T> Randomize<T>(this IEnumerable<T> enumerator)
        {
            return enumerator.OrderBy(x => _random.NextDouble());
        }
    }
}

