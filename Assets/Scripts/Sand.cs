﻿using System;
using UnityEngine;
using AssemblyCSharp.Scripts.Containers;

namespace AssemblyCSharp.Scripts
{
    public class Sand
    {
        public readonly Color32 Color;
        public int ColorVariance = 8;
        public bool ColorVarianceRgb = true;
        public bool ColorVarianceAlpha = false;
        public Buffer2d ColorVarianceMap = GenerateRandomVariance(32);

        private bool _idSet = false;
        private byte _id;
        public byte Id
        {
            get{return _id;}
            set
            {
                if (_idSet)
                    throw new InvalidOperationException("Already set");
                _id = value;
                _idSet = true;
            }
        }

        public virtual int Density
        {
            get
            {
                return 0;
            }
        }

        public virtual bool SimulateImmediately
        {
            get{return false;}
        }

        public virtual bool Empty
        {
            get{return false;}
        }

        public virtual bool Deadly
        {
            get{return false;}
        }

        public virtual bool Flammable
        {
            get { return false; }
        }

        public virtual bool Climbable
        {
            get { return false; }
        }

        /// <summary>
        /// Sound played on user-interactive place
        /// </summary>
        /// <value>The sfx place.</value>
        public virtual string SfxPlace
        {
            get{return "sfx/place";}
        }

        /// <summary>
        /// Looping sound played during simulation
        /// </summary>
        /// <value>The sfx simulate.</value>
        public virtual string SfxSimulate
        {
            get{return null;}
        }

        /// <summary>
        /// Sound played when user moves through or near sand
        /// </summary>
        /// <value>The sfx movement.</value>
        public virtual string SfxMovement
        {
            get{return null;}
        }

        protected Sand(Color32 color)
        {
            this.Color = color;
        }

        public virtual bool Simulate(int x, int y, SandWorld world)
        {
            return false;
        }

        protected void Swap(int x, int y, int x2, int y2, SandWorld world)
        {
            Sand s = world[x2, y2];
            world[x2, y2] = this;
            world[x, y] = s;
        }

        private string _name;
        public virtual string Name
        {
            get{return _name ?? (_name = this.GetType().Name);}
        }

        private string _typeName;
        public override string ToString()
        {
            return string.Format("{0}({1})", _typeName ?? (_typeName = this.GetType().Name), Id);
        }

        public static Buffer2d GenerateRandomVariance(int size)
        {
            var buffer = new Buffer2d(size, size);

            var rand = new System.Random(0);
            for (int y=0; y<buffer.Height; ++y)
            {
                for (int x=0; x<buffer.Width; ++x)
                {
                    buffer[x, y] = (byte)rand.Next(0, 255);
                }
            }

            return buffer;
        }
    }
}

