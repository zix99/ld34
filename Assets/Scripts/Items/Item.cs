﻿using UnityEngine;
using UnityEngine.UI;

namespace AssemblyCSharp.Scripts.Items
{
    public class Item
    {
        private readonly Image _uiItem;
        private readonly Sand _sand;

        public Item(Image UIItem, Sand sand, int quantity)
        {
            _uiItem = UIItem;
            _quantityText = _uiItem.transform.FindChild("Quantity").GetComponent<Text>();
            _selected = _uiItem.transform.FindChild("Select").GetComponent<Image>();
            _sand = sand;

            _uiItem.color = _sand.Color;

            _uiItem.transform.FindChild("Name").GetComponent<Text>().text = sand.Name;

            Quantity = quantity;
        }

        public Sand Sand { get { return _sand; } }

        public Image UiItem{get{return _uiItem;}}

        private int _quantity;
        private Text _quantityText;
        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
                _quantityText.text = value.ToString();
            }
        }

        private Image _selected;
        public bool Selected
        {
            get
            {
                return _selected.enabled;
            }
            set
            {
                _selected.enabled = value;
            }
        }
    }
}
