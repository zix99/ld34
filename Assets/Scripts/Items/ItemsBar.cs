﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AssemblyCSharp.Scripts.Items
{
    public class ItemsBar : MonoBehaviour
    {
        public Image UIItem;

        private readonly List<Item> _items = new List<Item>();

        private readonly KeyCode[] _numkeys = new [] { KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9 };

        private int _selectedIndex;

        private Text _power;
        private Text _totalScore;

        public void Start()
        {
            _power = this.transform.Find("Power").GetComponent<Text>();
            _totalScore = this.transform.Find("TotalScore").GetComponent<Text>();
        }

        public void AddItem(Sand sand, int quantity)
        {
            var item = Instantiate(UIItem);
            item.rectTransform.SetParent(this.transform, false);
            item.rectTransform.anchoredPosition = new Vector2(_items.Count * 100f + 50f, 0f);
            _items.Add(new Item(item, sand, quantity));
        }

        public void Clear()
        {
            _selectedIndex = 0;
            foreach(var item in _items)
            {
                item.UiItem.transform.DetachChildren();
                Destroy(item.UiItem);
            }
            _items.Clear();
        }

        public void SetPower(int power)
        {
            _power.text = power.ToString();
        }

        public void SetScore(int val)
        {
            _totalScore.text = string.Format("{0:D5}", val);
        }

        public Item Selected
        {
            get
            {
                if (_items.Count == 0)
                    return null;
                return _items[_selectedIndex];
            }
        }    

        public void Update()
        {
            if (_items.Count == 0)
                return;

            for(int i = 0; i < _numkeys.Length && i < _items.Count; i++)
            {
                if(Input.GetKeyDown(_numkeys[i]))
                {
                    _items[_selectedIndex].Selected = false;
                    _selectedIndex = i;
                }
            }
             
            if(Input.GetAxis("Mouse ScrollWheel") > 0 && _selectedIndex + 1 < _items.Count)
            {
                _items[_selectedIndex].Selected = false;
                _selectedIndex++;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0 && _selectedIndex - 1 >= 0)
            {
                _items[_selectedIndex].Selected = false;
                _selectedIndex--;
            }

            _items[_selectedIndex].Selected = true;
        }
    }
}