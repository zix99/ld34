﻿using System;
using UnityEngine;
using System.Threading;
using Diag = System.Diagnostics;
using AssemblyCSharp.Scripts.Sands;
using AssemblyCSharp.Scripts.Containers;

namespace AssemblyCSharp.Scripts.Jobs
{
    public class SandTextureRenderer : JobBase
    {
        private readonly MeshRenderer _mesh;
        private readonly Texture2D _texture;
        private readonly object _renderLock = new object();
        private readonly Color32[] _colorBuffer;
        private readonly SandWorld _world;

        public readonly int Width, Height;

        private Vector2 _currOffset;
        private Vector2 _targetOffset;

        #if UNITY_WEBGL
        private const int TARGET_FPS = 40;
        private const int SCREEN_RATIO = 4;
        #else
        private const int TARGET_FPS = 30;
        private const int SCREEN_RATIO = 3;
        #endif

        public SandTextureRenderer(MeshRenderer mesh, SandWorld world)
            :base("Sand renderer", TARGET_FPS)
        {
            _world = world;
            _mesh = mesh;

            _texture = new Texture2D(Screen.width / SCREEN_RATIO, Screen.height / SCREEN_RATIO, TextureFormat.RGBA32, false, false);
            _texture.filterMode = FilterMode.Point;
            _mesh.material.mainTexture = _texture;

            _colorBuffer = new Color32[_texture.width * _texture.height];
            this.Width = _texture.width;
            this.Height = _texture.height;
        }

        public void Recenter(int x, int y)
        {
            _targetOffset = new Vector2(x - this.Width/2, y - this.Height/2);
            _targetOffset.y = Math.Max(_targetOffset.y, -this.Height/6);
        }

        public Point2i UVToSandCoord(Vector2 uv)
        {
            var x = (int)((1 - uv.x) * this.Width) + (int)_currOffset.x;
            var y = (int)((1 - uv.y) * this.Height) + (int)_currOffset.y;

            return new Point2i(x, y);
        }

        protected override void Tick()
        {
            //Do some math
            _currOffset = Vector3.Lerp(_currOffset, _targetOffset, 0.05f);

            int renderOffsetX = (int)_currOffset.x;
            int renderOffsetY = (int)_currOffset.y;

            //Render
            lock(_renderLock)
            {
                for (int y=0; y<this.Height; ++y)
                {
                    for (int x=0; x<this.Width; ++x)
                    {
                        int wX = x + renderOffsetX;
                        int wY = y + renderOffsetY;
                        var block = _world[wX, wY];

                        var color = block.Color;
                        if (block.ColorVariance > 0)
                        {
                            int variance = (block.ColorVarianceMap.GetWrapped(wX, wY) % (block.ColorVariance*2)) - block.ColorVariance;
                            if (block.ColorVarianceRgb)
                            {
                                color.r = (byte)Mathf.Clamp(color.r + variance, 0, 255);
                                color.g = (byte)Mathf.Clamp(color.g + variance, 0, 255);
                                color.b = (byte)Mathf.Clamp(color.b + variance, 0, 255);
                            }
                            if (block.ColorVarianceAlpha)
                            {
                                color.a = (byte)Mathf.Clamp(color.a + variance, 0, 255);
                            }
                        }

                        _colorBuffer[(this.Height - y - 1)*this.Width+(this.Width - x - 1)] = color;
                    }
                }
            }
        }

        public void Apply()
        {
            
            float pixelWidth = this._mesh.bounds.size.x * MathHelper.Fraction(_currOffset.x) / this.Width;
            float pixelHeight = this._mesh.bounds.size.y * MathHelper.Fraction(_currOffset.y) / this.Height;

            this._mesh.transform.position = new Vector3(-pixelWidth, -pixelHeight, 0f);

            lock(_renderLock)
            {
                _texture.SetPixels32(_colorBuffer);
                _texture.Apply();
            }
        }
    }
}

