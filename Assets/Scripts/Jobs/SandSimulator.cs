﻿using AssemblyCSharp.Scripts.Sands;
using System.Linq;
using System.Collections.Generic;

namespace AssemblyCSharp.Scripts.Jobs
{
    public class SandSimulator : JobBase
    {
        private static readonly System.Random _random = new System.Random();

        private readonly SandWorld _world;

        private readonly HashSet<Point2i> _activePoints = new HashSet<Point2i>();
        private readonly List<Point2i> _processList = new List<Point2i>(8192);
        private readonly object _activeLock = new object();

        public int ActiveCount
        {
            get{return _activePoints.Count;}
        }

        public int ListCount
        {
            get{return _processList.Count;}
        }

        private volatile bool _listen = true;
        public bool Listen
        {
            get{return _listen;}
            set{_listen = value;}
        }

        public SandSimulator(SandWorld world)
            :base("Simulator", 20)
        {
            _world = world;
            _world.OnModify += _world_OnModify;
            _world.OnModify += (SandMan.Get<Player>() as Player).WorldModify;
        }

        private void _world_OnModify (int x, int y, byte newId, byte oldId)
        {
            if (_listen)
                this.Touch(x, y);
        }

        protected override void Tick()
        { 
            int max = 10000;

            lock (_activeLock)
            {
                for (int i = _processList.Count - 1; i >= 0; --i)
                {
                    var pt = _processList[i];

                    if (_activePoints.Remove(pt))
                        _processList.RemoveAt(i);

                    if (_world[pt.X, pt.Y].Simulate(pt.X, pt.Y, _world))
                    {
                        AddPoint(pt.X, pt.Y);
                    }

                    --max;
                    if (max <= 0)
                        break;
                }
            }
        }

        public void Touch(int x, int y)
        {
            AddPoint(x, y);

            AddPoint(x+1, y);
            AddPoint(x-1, y);
            AddPoint(x, y+1);
            AddPoint(x, y-1);
        }

        private void AddPoint(int x, int y)
        {
            lock(_activeLock)
            {
                if (!_world[x,y].Empty && _activePoints.Add(new Point2i(x,y)))
                {
                    _processList.Insert(_random.Next(_processList.Count), new Point2i(x, y));
                }
            }
        }

        public void Clear()
        {
            lock(_activeLock)
            {
                _activePoints.Clear();
                _processList.Clear();
            }
        }
    }
}

