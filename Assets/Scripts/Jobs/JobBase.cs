﻿using System;
using System.Threading;
using UnityEngine;
using Diag = System.Diagnostics;

namespace AssemblyCSharp.Scripts.Jobs
{
    public abstract class JobBase
    {
        #if !UNITY_WEBGL
        private readonly Thread _thread;
        private readonly object _pause = new object();
        #endif

        private readonly string _name;
        private readonly int _fps;

        public int TargetFps
        {
            get{return _fps;}
        }

        private double _avgFrameTime;
        public double AvgFrameTime
        {
            get{return _avgFrameTime;}
        }

        public double MillisPerFrame
        {
            get{return 1000.0 / _fps;}
        }

        public JobBase(string name, int desiredFps)
        {
            _name = name;
            _fps = desiredFps;
            #if !UNITY_WEBGL
            _thread = new Thread(this.AsyncLoop);
            _thread.Name = name;
            _thread.IsBackground = true;
            #endif
        }

        public void Start()
        {
            Debug.Log("Starting thread " + _name);
            #if !UNITY_WEBGL
            _thread.Start();
            #endif
        }

        public void Pause()
        {
            Debug.Log("Pausing thread " + _name);
            #if !UNITY_WEBGL
            Monitor.Enter(_pause);
            #endif
        }

        public void Resume()
        {
            Debug.Log("Resuming thread " + _name);
            #if !UNITY_WEBGL
            Monitor.Exit(_pause);
            #endif
        }

        public void Stop()
        {
            Debug.Log("Stopping thread " + _name);
            #if !UNITY_WEBGL
            _thread.Interrupt();
            if (!_thread.Join(500))
            {
                _thread.Abort();
            }
            #endif
        }

        #if !UNITY_WEBGL
        private void AsyncLoop()
        {
            try
            {
                var sw = new Diag.Stopwatch();
                double targetTime = 1000.0 / _fps;

                while(true)
                {
                    lock(_pause)
                    {
                        sw.Start();
                        this.Tick();
                        sw.Stop();
                    }

                    _avgFrameTime = (_avgFrameTime + sw.ElapsedMilliseconds) / 2.0;

                    double remaining = targetTime - sw.ElapsedMilliseconds;
                    if (remaining > 0.0)
                    {
                        Thread.Sleep(TimeSpan.FromMilliseconds(remaining));
                    }
                    else
                    {
                        Debug.LogWarning(string.Format("Thread {0} going too slow: {1}ms", _name, remaining));
                        Thread.Sleep(0); //hack so interrupt works
                    }

                    sw.Reset();
                }
            }
            catch(ThreadInterruptedException)
            {
                Debug.Log(string.Format("Thread {0} interrupted", _name));
            }
            catch(Exception e)
            {
                Debug.LogError(string.Format("Exception: {0}\n{1}", e.Message, e.StackTrace));
            }
        }
        #endif

        #if UNITY_WEBGL
        private float _lastFrame = 0f;

        public void SyncUpdate()
        {
            if (Time.realtimeSinceStartup - _lastFrame >= 1f / _fps)
            {
                _lastFrame = Time.realtimeSinceStartup;

                float start = Time.realtimeSinceStartup;
                Tick();
                float end = Time.realtimeSinceStartup;

                _avgFrameTime = (_avgFrameTime + (end - start)*1000f) / 2.0;
            }
        }
        #endif

        protected abstract void Tick();
    }
}

