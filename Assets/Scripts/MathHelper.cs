﻿using System;

namespace AssemblyCSharp.Scripts
{
    public static class MathHelper
    {
        private static readonly System.Random _random = new System.Random();

        public static float Fraction(float val)
        {
            return val - (int)val;
        }

        public static int mmod(int i, int m)
        {
            return (i % m + m) % m;
        }

        public static bool Chance(float likelihood)
        {
            return _random.NextDouble() < likelihood;
        }

        public static int Random(int min, int max)
        {
            return _random.Next(min, max+1);
        }
    }
}

