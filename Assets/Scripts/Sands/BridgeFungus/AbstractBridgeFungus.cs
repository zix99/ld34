﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.BridgeFungus
{
    public class AbstractBridgeFungus : AbstractGrowth
    {

        protected AbstractBridgeFungus(Color32 color)
            :base(color)
        {
        }
    }
}

