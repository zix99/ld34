﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.BridgeFungus
{
    public class HangingBridgeFungus : AbstractBridgeFungus
    {
        public HangingBridgeFungus()
            :base(new Color32(220,255,100,255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if (world[x,y-1].Empty && MathHelper.Chance(0.5f))
            {
                world[x, y - 1] = this;
            }

            return false;
        }
    }
}

