﻿using System;
using AssemblyCSharp.Scripts.Sands.Roots;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.BridgeFungus
{
    public class AliveBridgeFungus : AbstractBridgeFungus, IRootReceiver
    {
        public AliveBridgeFungus()
            :base(new Color32(200,255,50,255))
        {
        }

        public override string SfxSimulate
        {
            get
            {
                return "sfx/growing";
            }
        }

        #region IRootReceiver implementation

        public Sand TransmitSand
        {
            get
            {
                return SandMan.Get<TransmittingBridgeFungus>();
            }
        }

        #endregion
    }
}

