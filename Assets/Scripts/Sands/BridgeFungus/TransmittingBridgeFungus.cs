﻿using System;
using System.Linq;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.BridgeFungus
{
    public class TransmittingBridgeFungus : AbstractBridgeFungus
    {
        public TransmittingBridgeFungus()
            :base(new Color32(220,255,80,255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            bool built = false;
            if (world[x+1,y].Empty)
            {
                world[x + 1, y] = SandMan.Get<AliveBridgeFungus>();
                built = true;
            }

            if (world[x-1,y].Empty)
            {
                world[x - 1, y] = SandMan.Get<AliveBridgeFungus>();
                built = true;
            }

            if (built)
            {
                if (world[x,y-1].Empty)
                {
                    world[x, y - 1] = SandMan.Get<HangingBridgeFungus>();
                }

                world[x, y] = SandMan.Get<AliveBridgeFungus>();
                return false;
            }

            int dirX = MathHelper.Chance(0.5f) ? -1 : 1;
            int curX = x;
            while(world[curX+dirX, y] is AliveBridgeFungus){curX += dirX;}
            if (curX != x)
            {
                Swap(x, y, curX, y, world);
            }
            /*
            foreach(var offset in Point2i.XAxis.Randomize())
            {
                if (world[x+offset.X,y] is AliveBridgeFungus)
                {
                    Swap(x, y, x + offset.X, y, world);
                    return false;
                }
            }*/

            return true;
        }
    }
}

