﻿using System;
using UnityEngine;
using AssemblyCSharp.Scripts.Sands.Roots;

namespace AssemblyCSharp.Scripts.Sands.BridgeFungus
{
    public class GrowingBridgeFungus : AbstractBridgeFungus
    {
        public GrowingBridgeFungus()
            :base(new Color32(220,255,100,255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            //Falling
            if (world[x,y-1].Empty)
            {
                Swap(x, y, x, y - 1, world);
                return false;
            }

            //If hit grass, plant seed
            if (world[x,y-1] is IGrowable)
            {
                world[x, y - 1] = SandMan.Get<TransmittingRoot>();
                return false;
            }

            //Spread!
            if (world[x+1,y].Empty)
            {
                world[x + 1, y] = SandMan.Get<AliveBridgeFungus>();
            }
            if (world[x-1,y].Empty)
            {
                world[x - 1, y] = SandMan.Get<AliveBridgeFungus>();
            }
            world[x, y] = SandMan.Get<AliveBridgeFungus>();

            return false;
        }

        public override string Name
        {
            get
            {
                return "Bridge Moss";
            }
        }
    }
}

