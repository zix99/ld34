﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Meta
{
    public class BrickWallBuilder : AbstractSolid
    {
        public BrickWallBuilder()
            :base(new Color32(250, 69, 33, 255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            const int MAX_DROP = 40;
            const int BUILD_HEIGHT = 15;
            const int WIDTH = 2;

            world[x, y] = SandMan.Empty;

            for (int s=x-WIDTH; s<=x+WIDTH; ++s)
            {

                for (int t=y+BUILD_HEIGHT; t>=y-MAX_DROP; --t)
                {
                    if (!world[s, t].Empty)
                        break;
                    world[s, t] = SandMan.Get<Bricks>();
                }
            }

            return false;
        }
    }
}

