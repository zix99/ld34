﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Meta
{
    public class WaterBucket : Sand
    {
        public WaterBucket()
            :base(new Color32(64, 64, 200, 150))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            const int SZ = 4;

            world[x, y] = SandMan.Empty;

            for (int s=x-SZ; s<=x+SZ; ++s)
            {
                for (int t=y-SZ; t<=y+SZ; ++t)
                {
                    if (world[s, t].Empty)
                        world[s, t] = SandMan.Get<Water>();
                }
            }

            return false;
        }
    }
}

