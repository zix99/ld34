﻿using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Water : AbstractLiquid
    {
        public Water()
            : base(new Color32(0, 64, 200, 150))
        {
        }
    }
}

