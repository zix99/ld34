﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    /// <summary>
    /// Class with helpers for the world
    /// </summary>
    public class AbstractGrowth : Sand
    {
        protected AbstractGrowth(Color32 color)
            :base(color)
        {
        }

        public override int Density
        {
            get
            {
                return 100;
            }
        }

        public override bool Flammable
        {
            get
            {
                return true;
            }
        }

    }
}

