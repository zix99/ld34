﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class DeadGrowth : AbstractPiling
    {
        public DeadGrowth()
            :base(new Color32(100, 100, 0, 255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            for (int i=0; i<Point2i.AllSides.Length; ++i)
            {
                var off = Point2i.AllSides[i];
                if (world[x + off.X, y + off.Y] is AbstractGrowth)
                    world[x + off.X, y + off.Y] = this;
            }

            world[x, y] = SandMan.Get<Dirt>();

            return base.Simulate(x, y, world);
        }
    }
}

