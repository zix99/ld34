﻿using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Exit : Sand
    {
        private GameObject _go;

        public Exit()
            :base(new Color32(255,255,255,128))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            var p = SandMan.Get<Player>();
            if(world[x + 1, y - 1] == p || world[x + 1, y] == p || world[x + 1, y + 1] == p
                || world[x, y - 1] == p || world[x, y + 1] == p
                || world[x - 1, y - 1] == p || world[x - 1, y] == p || world[x - 1, y + 1] == p)
            {
                Events.LevelCompeted();
            }
            return true;
        }
    }
}

