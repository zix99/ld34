﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Nothing : Sand
    {
        public Nothing()
            :base(new Color32(0, 0, 0, 0))
        {
            this.ColorVariance = 0;
        }

        public override bool Empty
        {
            get
            {
                return true;
            }
        }
    }
}

