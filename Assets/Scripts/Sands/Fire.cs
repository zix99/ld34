﻿namespace AssemblyCSharp.Scripts.Sands
{
    public class Fire : Sand
    {
        public Fire() : base(new UnityEngine.Color32(255, 0, 100, 255)) { }

        public override bool Deadly
        {
            get
            {
                return true;
            }
        }

        public override string SfxSimulate
        {
            get
            {
                return "sfx/fire";
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if(world[x, y + 1].Empty && MathHelper.Chance(0.6f))
            {
                world[x, y + 1] = this;
            }

            for (int s = x - 1; s <= x + 1; ++s)
            {
                for (int t = y - 1; t <= y + 1; ++t)
                {
                    if (s == x && t == y)
                        continue;

                    if (world[s, t].Flammable)
                    {
                        world[s, t] = this;
                        return true;
                    }
                }
            }


            world[x, y] = SandMan.Empty;
            return false;

        }
    }
}
