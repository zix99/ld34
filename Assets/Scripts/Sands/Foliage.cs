﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Foliage : AbstractGrowth
    {
        public Foliage()
            :base(new Color32(0, 200, 0, 255))
        {
        }
    }
}

