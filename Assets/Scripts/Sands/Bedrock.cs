﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Bedrock : Sand
    {
        public Bedrock()
            :base(new Color32(100, 100, 100, 255))
        {
            this.ColorVariance = 24;
        }

        public override int Density
        {
            get
            {
                return int.MaxValue;
            }
        }
    }
}

