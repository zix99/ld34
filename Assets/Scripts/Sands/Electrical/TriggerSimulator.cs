﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Electrical
{
    public class TriggerSimulator : AbstractElectrical
    {
        public TriggerSimulator()
            :base(new Color32(10, 10, 100, 255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            //If we were activated, send a signal to any near by conductive
            this.PowerNearby(x, y, world);
            return false;
        }
    }
}

