﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Electrical
{
    public class ReactorSpark : AbstractElectrical, IConductive
    {
        public ReactorSpark()
            :base(new Color32(255, 0, 100, 255))
        {
        }


        #region IConductive implementation
        public void Power(int x, int y, SandWorld world)
        {
            foreach(var offset in Point2i.AllSides)
            {
                if (world[x + offset.X, y + offset.Y].Flammable)
                    world[x + offset.X, y + offset.Y] = SandMan.Get<Fire>();
            }
        }
        #endregion
    }
}

