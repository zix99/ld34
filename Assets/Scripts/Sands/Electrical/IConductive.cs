﻿using System;

namespace AssemblyCSharp.Scripts.Sands.Electrical
{
    public interface IConductive
    {
        void Power(int x, int y, SandWorld world);
    }
}

