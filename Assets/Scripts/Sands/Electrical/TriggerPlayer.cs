﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Electrical
{
    public class TriggerPlayer : AbstractElectrical
    {
        public TriggerPlayer()
            :base(new Color32(10,20,100,255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            foreach(var offset in Point2i.AxisSides)
            {
                if (world[x+offset.X, y+offset.Y] is Player)
                {
                    this.PowerNearby(x, y, world);
                    break;
                }
            }

            return false;
        }
    }
}

