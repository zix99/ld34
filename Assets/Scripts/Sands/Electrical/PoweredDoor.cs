﻿using System;
using UnityEngine;
using AssemblyCSharp.Scripts.Sands.Traps;

namespace AssemblyCSharp.Scripts.Sands.Electrical
{
    public class PoweredDoor : AbstractElectrical, IConductive
    {
        public PoweredDoor()
            : base(new Color32(80, 80, 100, 255))
        {
        }

        #region IConductive implementation

        public void Power(int x, int y, SandWorld world)
        {
            world[x, y] = SandMan.Get<DisappearingWall>();
            this.PowerNearby(x, y, world);
        }

        #endregion
    }
}

