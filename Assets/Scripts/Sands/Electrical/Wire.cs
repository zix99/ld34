﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Electrical
{
    public class Wire : AbstractElectrical, IConductive
    {
        public Wire()
            :base(new Color32(175, 175, 100, 255))
        {
        }

        public void Power(int x, int y, SandWorld world)
        {
            world[x, y] = SandMan.Get<WireOn>();
            this.PowerNearby(x, y, world);
        }
    }
}

