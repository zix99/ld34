﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Electrical
{
    public class AbstractElectrical : Sand
    {
        protected AbstractElectrical(Color32 color)
            :base(color)
        {
        }

        public override int Density
        {
            get
            {
                return 9999;
            }
        }

        public void PowerNearby(int x, int y, SandWorld world)
        {
            foreach(var offset in Point2i.AxisSides)
            {
                var conductive = world[x + offset.X, y + offset.Y] as IConductive;
                if (conductive != null)
                    conductive.Power(x + offset.X, y + offset.Y, world);
            }
        }

        public override string SfxSimulate
        {
            get
            {
                return "sfx/zap";
            }
        }
    }
}

