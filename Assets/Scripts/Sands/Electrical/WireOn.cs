﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Electrical
{
    public class WireOn : AbstractElectrical
    {
        public WireOn()
            :base(new Color32(255,255,100,255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            world[x, y] = SandMan.Get<Wire>();
            return false;
        }
    }
}

