﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Vegetation
{
    public class TreeSeed : AbstractGrowth
    {
        public TreeSeed()
            :base(new Color32(200, 150, 0, 255))
        {
            
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if (world[x,y-1].Empty)
            {
                Swap(x, y, x, y - 1, world);
                return false;
            }

            if (world[x,y-1] is IGrowable)
            {
                //Grow tree
                world[x + 1, y] = world[x - 1, y] = world[x, y] = SandMan.Get<TreeTrunk>();
            }

            return false;
        }

        public override bool SimulateImmediately
        {
            get
            {
                return true;
            }
        }
    }
}

