﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Vegetation
{
    public class TreeFoliageGrowth : AbstractGrowth
    {
        private static readonly Point2i[] _hat = new Point2i[]
        {
            new Point2i(-1, 1), new Point2i(0, 1), new Point2i(1, 1)
        };

        public TreeFoliageGrowth()
            :base(new Color32(25, 200, 25, 255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            const int RADIUS = 8;

            world[x, y] = SandMan.Get<Foliage>();

            Point2i? closest = world.FindWhere(x, y, RADIUS, p => p is Wood);

            if (closest.HasValue)
            {
                if ((closest.Value - new Point2i(x, y)).Length <= RADIUS || MathHelper.Chance(0.7f))
                {
                    foreach (var offset in _hat)
                    {
                        if (world[x + offset.X, y + offset.Y].Empty)
                            world[x + offset.X, y + offset.Y] = this;
                    }
                }
            }

            return false;
        }
    }
}

