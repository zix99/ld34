﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Vegetation
{
    public class FlowerSeed : AbstractGrowth
    {
        private static readonly Point2i[] _shoots = new Point2i[]
        {
            new Point2i(1, 1),
            new Point2i(-1, 1),
            new Point2i(0, 1)
        };

        public FlowerSeed()
            :base(new Color32(200, 150, 50, 255))
        {

        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if (world[x,y-1].Empty)
            {
                Swap(x, y, x, y - 1, world);
                return false;
            }

            if (world[x,y-1] is IGrowable)
            {
                world[x, y] = SandMan.Get<Foliage>();

                const int SZ = 3;

                //Grow tree
                for (int d = 0; d <= SZ; ++d)
                {
                    foreach (var offset in _shoots)
                    {
                        var pt = new Point2i(x + offset.X * d, y + offset.Y * d);

                        if (world[pt.X, pt.Y].Empty)
                        {
                            if (d < SZ)
                                world[pt.X, pt.Y] = SandMan.Get<Foliage>();
                            else
                                world[pt.X, pt.Y] = SandMan.Get<FlowerYellow>();
                        }
                    }
                }
            }

            return false;
        }

        public override bool SimulateImmediately
        {
            get
            {
                return true;
            }
        }
    }
}

