﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Vegetation
{
    public class TreeTrunk : AbstractGrowth
    {
        public TreeTrunk()
            :base(new Color32(120, 50, 16, 255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            world[x, y] = SandMan.Get<Wood>();

            if (!world[x,y+1].Empty)
            {
                return false; //something is above me, stop growing
            }

            //Count below me
            int currHeight = 0;
            while(world[x,y-currHeight] is Wood){currHeight++;}

            const int HEIGHT = 20;
            if (currHeight < HEIGHT)
            {
                //Grow some more
                world[x,y+1] = this;
                return false;
            }

            if (currHeight >= HEIGHT)
            {
                world[x, y + 1] = SandMan.Get<TreeFoliageGrowth>();
                return false;
            }

            return false;
        }
    }
}

