﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Traps
{
    public class TrapWall : AbstractSolid
    {
        public TrapWall()
            :base(new Color32(150, 150, 150, 255))
        {
            this.ColorVarianceMap = Bricks.BuildBrickVarianceBuffer();
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if (world[x,y+1].Empty)
            {
                world[x, y + 1] = this;
            }
            return false;
        }
    }
}

