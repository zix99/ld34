﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Traps
{
    public class DisappearingWall : AbstractSolid
    {
        public DisappearingWall()
            :base(new Color32(160, 160, 160, 255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            world[x, y] = SandMan.Empty;
            return false;
        }
    }
}

