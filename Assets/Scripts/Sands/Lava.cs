﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Lava : AbstractLiquid
    {
        public Lava()
            : base(new Color32(255, 111, 54, 220))
        {
        }

        public override bool Deadly
        {
            get
            {
                return true;
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {

            foreach(var offset in Point2i.AxisSides)
            {
                if (world[x+offset.X, y+offset.Y] is Water)
                {
                    world[x, y] = SandMan.Get<Rock>();
                    return false;
                }

                if (world[x+offset.X, y+offset.Y].Flammable)
                {
                    world[x+offset.X, y+offset.Y] = SandMan.Get<Fire>();
                    return true;
                }
            }

            base.Simulate(x, y, world);
            return false;
        }
    }
}

