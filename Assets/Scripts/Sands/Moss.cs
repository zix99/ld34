﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Moss : AbstractGrowth
    {
        public Moss()
            : base(new Color32(50, 255, 50, 255))
        {
        }

        public override string SfxSimulate
        {
            get
            {
                return "sfx/growing";
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if (world[x, y - 1].Empty)
            {
                Swap(x, y, x, y - 1, world);
                return false;
            }

            if (world.Any(x,y,2,sand => sand.Empty) && world.AnyOther(x,y,1,p => p is Water))
            {
                world[x + 1, y] = this;
                world[x + 1, y + 1] = this;
                world[x - 1, y] = this;
                world[x - 1, y + 1] = this;
                world[x, y - 1] = this;
            }

            if (world.Any(x,y,8,sand => sand.Empty) && world[x, y - 1] == SandMan.Get<Water>() && MathHelper.Chance(0.3f))
            {
                world[x, y - 1] = this;
            }

            return false;
        }
    }
}

