﻿using System;
using UnityEngine;
using AssemblyCSharp.Scripts.Containers;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Bricks : AbstractSolid
    {
        public Bricks()
            :base(new Color32(200, 69, 33, 255))
        {
            this.ColorVarianceMap = BuildBrickVarianceBuffer();
        }

        public static Buffer2d BuildBrickVarianceBuffer()
        {
            var buffer = new Buffer2d(16, 16);
            for (int x=0; x<buffer.Width; ++x)
            {
                for (int y=0; y<buffer.Height; ++y)
                {
                    if (x % 4 == 0 && (y / 3) % 2 == 0)
                        buffer[x, y] = 12;
                    else if (x % 4 == 2 && (y / 3) % 2 == 1)
                        buffer[x, y] = 12;
                    else if (y % 3 == 0)
                        buffer[x, y] = 16;
                    else
                        buffer[x, y] = 0;
                }
            }
            return buffer;
        }
    }
}

