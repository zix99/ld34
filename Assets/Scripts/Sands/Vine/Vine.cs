﻿namespace AssemblyCSharp.Scripts.Sands.Vine
{
    public class Vine : Sand
    {
        public Vine() : base(new UnityEngine.Color32(15, 170, 15, 255)) { }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if(world[x, y - 1].Empty)
            {
                Swap(x, y, x, y - 1, world);
                return false;
            }

            if(world[x, y - 1] == SandMan.Get<Grass>())
            {
                world[x, y] = SandMan.Get<VineSearcher>();
                world[x, y - 1] = SandMan.Get<VineRoot>();
            }

            return false;
        }
    }
}
