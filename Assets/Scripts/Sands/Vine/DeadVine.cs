﻿namespace AssemblyCSharp.Scripts.Sands.Vine
{
    public class DeadVine : AbstractPiling
    {
        public DeadVine() : base(new UnityEngine.Color32(40, 10, 10, 255)) { }
    }
}
