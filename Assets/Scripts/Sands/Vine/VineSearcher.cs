﻿namespace AssemblyCSharp.Scripts.Sands.Vine
{
    public class VineSearcher : VineBase
    {
        public VineSearcher() : base(new UnityEngine.Color32(15, 170, 15, 255)) { }

        public override string SfxMovement
        {
            get
            {
                return "sfx/growing";
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if(world.Any(x, y, 1, s => s is Bricks) || world.Any(x, y, 1, s => s is VineActive))
            {
                world[x, y] = SandMan.Get<VineActive>();
                return true;
            }

            if(!(world[x, y - 1] == SandMan.Get<Grass>() || world[x, y - 1] == this || world[x, y - 1] == SandMan.Get<VineRoot>()))
            {
                return false;
            }

            if(world.Count(x + 1, y, 1, s => s is VineSearcher || s is VineSearched) < 2 )
            {
                if (world[x + 1, y - 1].Empty)
                {
                    world[x + 1, y - 1] = this;
                }
                else if (world[x + 1, y].Empty)
                {
                    world[x + 1, y] = this;
                }
                else if (world[x + 1, y + 1].Empty)
                {
                    world[x + 1, y + 1] = this;
                }
            }


            if (world.Count(x - 1, y, 1, s => s is VineSearcher || s is VineSearched) < 2)
            {
                if (world[x - 1, y - 1].Empty)
                {
                    world[x - 1, y - 1] = this;
                }
                else if (world[x - 1, y].Empty)
                {
                    world[x - 1, y] = this;
                }
                else if (world[x - 1, y + 1].Empty)
                {
                    world[x - 1, y + 1] = this;
                }
            }

            if(world[x, y + 1].Empty && MathHelper.Chance(0.6f))
            {
                world[x, y + 1] = this;
            }
            else
            {
                world[x, y] = SandMan.Get<VineSearched>();
            }

            return false;
        }
    }
}
