﻿using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Vine
{
    public class VineBase : Sand
    {
        public VineBase(Color32 color) : base(color) { }

        public override int Density
        {
            get
            {
                return 100;
            }
        }
    }
}
