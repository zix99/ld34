﻿namespace AssemblyCSharp.Scripts.Sands.Vine
{
    public class VineRoot : VineBase
    {
        public VineRoot() : base(new UnityEngine.Color32(15, 170, 15, 255)) { }

        public override bool Simulate(int x, int y, SandWorld world)
        {

            return false;
        }
    }
}
