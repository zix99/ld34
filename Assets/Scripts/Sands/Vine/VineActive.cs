﻿using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Vine
{
    public class VineActive : VineBase
    {
        public VineActive() : base(new UnityEngine.Color32(15, 170, 15, 255)) { }

        public override string SfxMovement
        {
            get
            {
                return "sfx/growing";
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            var target = Mathf.PerlinNoise(x / 10f, y / 10f);

            bool die = true;
            for (int s = x - 1; s <= x + 1; ++s)
            {
                for (int t = y - 1; t <= y + 1; ++t)
                {
                    if (s == x && t == y)
                        continue;

                    if(world[s, t] == SandMan.Get<Bricks>())
                    {
                        die = false;
                        if (world[s, t] == SandMan.Get<Bricks>() && Mathf.Abs(Mathf.PerlinNoise(s / 10f, t / 10f) - target) < 0.058f)
                        {
                            world[s, t] = this;
                        }
                        else
                        {
                            world[s, t] = SandMan.Get<Dirt>();
                        }
                    }                    
                }
            }

            if (die || world.Any(x, y, 1, s => s is DeadVine))
            {
                world[x, y] = SandMan.Get<VineSearched>();
                return true;
            }

            return false;
        }
    }
}