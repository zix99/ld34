﻿namespace AssemblyCSharp.Scripts.Sands.Vine
{
    public class VineSearched : Sand
    {
        public VineSearched() : base(new UnityEngine.Color32(15, 170, 15, 255)) { }

        public override int Density
        {
            get
            {
                return 100;
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if(!world.Any(x, y, 3, s => s is VineActive || s is VineSearcher) || world.Any(x, y, 1, s => s is DeadVine))
            {
                world[x, y] = SandMan.Get<DeadVine>();
            }

            return false;
        }

    }
}
