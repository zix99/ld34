﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Roots
{
    public class AliveRoot : AbstractRoot
    {
        public AliveRoot()
            :base(new Color32(150, 100, 50, 255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {

            foreach(var off in Point2i.AllSides)
            {
                var block = world[x + off.X, y + off.Y];
                if (block is IGrowable)
                {
                    world[x + off.X, y + off.Y] = SandMan.Get<TransmittingRoot>();
                    return true;
                }
            }

            return false;
        }
    }
}

