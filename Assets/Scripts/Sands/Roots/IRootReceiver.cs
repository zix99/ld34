﻿using System;

namespace AssemblyCSharp.Scripts.Sands.Roots
{
    public interface IRootReceiver
    {
        Sand TransmitSand{get;}
    }
}

