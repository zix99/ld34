﻿using System;
using System.Linq;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Roots
{
    public class TransmittingRoot : AbstractRoot
    {
        private static readonly Point2i[] _hat = new Point2i[]{
            new Point2i(-1, 1), new Point2i(0, 1), new Point2i(1, 1),
            new Point2i(-1, 0), new Point2i(1, 0),
        };

        public TransmittingRoot()
            :base(new Color32(150,120,50,255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {

            //Transmit up the fungus
            var tgt = world[x,y+1] as IRootReceiver;
            if (tgt != null)
            {
                world[x, y + 1] = tgt.TransmitSand;
                world[x, y] = SandMan.Get<AliveRoot>();
                return false;
            }

            //Transmit to antoher root
            foreach(var off in _hat.OrderBy(t => MathHelper.Random(0,100)))
            {
                if (world[x+off.X, y+off.Y] is AliveRoot)
                {
                    Swap(x, y, x + off.X, y + off.Y, world);
                    return false;
                }
            }

            return false;
        }
    }
}

