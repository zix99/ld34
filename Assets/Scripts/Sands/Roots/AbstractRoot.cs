﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Roots
{
    public class AbstractRoot : AbstractGrowth
    {
        protected AbstractRoot(Color32 color)
            :base(color)
        {
        }
    }
}

