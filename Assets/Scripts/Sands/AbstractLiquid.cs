﻿using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class AbstractLiquid : Sand
    {
        private static readonly System.Random _random = new System.Random();

        protected AbstractLiquid(Color32 color)
            :base(color)
        {
            this.ColorVarianceAlpha = true;
        }

        public override int Density
        {
            get
            {
                return 1;
            }
        }

        public override string SfxPlace
        {
            get
            {
                return "sfx/water-drop";
            }
        }

        public override string SfxMovement
        {
            get
            {
                return "sfx/water-woosh";
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if (world[x, y - 1].Density < Density)
            {
                Swap(x, y, x, y - 1, world);
                return false;
            }
            else if (world[x - 1, y - 1].Density < Density && world[x + 1, y - 1].Density < Density)
            {
                if (_random.NextDouble() > 0.5)
                {
                    //Go right
                    Swap(x, y, x + 1, y - 1, world);
                    return false;
                }
                else
                {
                    //Go left
                    Swap(x, y, x - 1, y - 1, world);
                    return false;
                }
            }
            else if (world[x - 1, y - 1].Density < Density)
            {
                Swap(x, y, x - 1, y - 1, world);
                return false;
            }
            else if (world[x + 1, y - 1].Density < Density)
            {
                Swap(x, y, x + 1, y - 1, world);
                return false;
            }

            if (world[x, y - 1] != this)
            {
                return true;
            }

            var right = int.MaxValue;
            var left = int.MaxValue;
            for(int i = 2; world[x + i, y].Density < Density; i++)
            {
                if(world[x + i, y - 1].Density < Density)
                {
                    right = i;
                    break;
                }
            }

            for(int i = 1; world[x - i, y].Density < Density; i++)
            {
                if(world[x - i, y - 1].Density < Density)
                {
                    left = i;
                    break;
                }
            }

            if(right == int.MaxValue && left == int.MaxValue)
            {
                return false;
            }

            if (right < left)
            {
                Swap(x, y, x + right, y - 1, world);
            }
            else if (left < right)
            {
                Swap(x, y, x - left, y - 1, world);
            }
            else
            {
                if (_random.NextDouble() > 0.5)
                {
                    Swap(x, y, x + right, y - 1, world);
                }
                else
                {
                    Swap(x, y, x - left, y - 1, world);
                }
            }

            return false;
        }
    }
}

