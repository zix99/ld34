﻿namespace AssemblyCSharp.Scripts.Sands
{
    public class PlayerSleep : Sand
    {
        public PlayerSleep() : base(new UnityEngine.Color32(255, 140, 140, 255)) { }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if(world.Any(x, y, 1, s => s is Player))
            {
                world[x, y] = SandMan.Get<Player>();
                return true;
            }

            return false;
        }
    }
}
