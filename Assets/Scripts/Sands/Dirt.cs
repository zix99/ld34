﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Dirt : AbstractPiling, IGrowable
    {
        public Dirt()
            :base(new Color32(128,64,0, 255))
        {
        }
    }
}

