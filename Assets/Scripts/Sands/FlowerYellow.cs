﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class FlowerYellow : AbstractGrowth
    {
        public FlowerYellow()
            :base(new Color32(255,255,0,255))
        {
        }
    }
}

