﻿using System;
using System.Linq;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Fungus
{
    public class TransmittingFungus : AbstractFungus
    {
        private static readonly Point2i[] _hat = new Point2i[]{
            new Point2i(-1, 1), new Point2i(0, 1), new Point2i(1, 1),
            new Point2i(-1, 0), new Point2i(1, 0),
        };

        public TransmittingFungus()
            :base(new Color32(230, 230, 255, 255))
        {
        }

        public override bool Climbable
        {
            get
            {
                return true;
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            //Falling
            if (!this.IsAttached(x, y, world))
            {
                world[x, y] = SandMan.Get<DeadGrowth>();
                return false;
            }
                
            foreach(var off in _hat.OrderBy(t => MathHelper.Random(0,100)))
            {
                if (world[x+off.X, y+off.Y] is AliveFungus)
                {
                    Swap(x, y, x + off.X, y + off.Y, world);
                    return false;
                }
            }

            //IF reachewd here, end of the line, let's grow!
            world[x,y] = SandMan.Get<GrowingFungus>();
            return false;
        }
    }
}

