﻿using System;
using System.Linq;
using UnityEngine;
using AssemblyCSharp.Scripts.Sands.Roots;

namespace AssemblyCSharp.Scripts.Sands.Fungus
{
    public class GrowingFungus : AbstractFungus
    {
        public GrowingFungus()
            :base(new Color32(200, 200, 220, 255))
        {
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            //Falling
            if (this.BelowEmpty(x,y,world))
            {
                Swap(x, y, x, y - 1, world);
                return false;
            }

            //Touch something other than grass and death be onto you
            if(!this.IsAttached(x,y,world))
            {
                world[x, y] = SandMan.Empty;
                return false;
            }

            //Grass is below, plant a root
            if (world[x, y - 1] is IGrowable)
                world[x, y - 1] = SandMan.Get<TransmittingRoot>();

            //Grow!

            world[x, y] = SandMan.Get<AliveFungus>();

            int count = MathHelper.Random(1, 2);
            for (int i = 0; i < count; ++i)
            {
                int subX = MathHelper.Chance(0.5f) ? -1 : 1;
                int subY = MathHelper.Chance(0.6f) ? 1 : 0;
                if (world[x+subX, y+subY].Empty)
                    world[x + subX, y + subY] = SandMan.Get<AliveFungus>();
            }

            return false;
        }

        public override string Name
        {
            get
            {
                return "Fungus";
            }
        }
    }
}

