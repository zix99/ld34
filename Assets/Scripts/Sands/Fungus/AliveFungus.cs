﻿using System;
using UnityEngine;
using AssemblyCSharp.Scripts.Sands.Roots;

namespace AssemblyCSharp.Scripts.Sands.Fungus
{
    public class AliveFungus : AbstractFungus, IRootReceiver
    {
        public AliveFungus()
            :base(new Color32(200, 200, 210, 255))
        {
            
        }

        public override bool Climbable
        {
            get
            {
                return true;
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            //Falling
            if (!this.IsAttached(x, y, world))
            {
                world[x, y] = SandMan.Get<DeadGrowth>();
                return false;
            }

            return false;
        }

        #region IRootReceiver implementation

        public Sand TransmitSand
        {
            get
            {
                return SandMan.Get<TransmittingFungus>();
            }
        }

        #endregion
    }
}

