﻿using System;
using System.Linq;
using UnityEngine;
using AssemblyCSharp.Scripts.Sands.Roots;

namespace AssemblyCSharp.Scripts.Sands.Fungus
{
    public class AbstractFungus : AbstractGrowth
    {
        private static readonly Point2i[] _basket = new Point2i[]
            {
                new Point2i(-1, 0), new Point2i(1, 0),
                new Point2i(-1, -1), new Point2i(0, -1), new Point2i(1, -1)
            };

        public AbstractFungus(Color32 color)
            :base(color)
        {
        }

        protected bool IsAttached(int x, int y, SandWorld world)
        {
            return _basket.Any(offset =>
                {
                    var block = world[x + offset.X, y + offset.Y];
                    return block is IGrowable || block is AbstractFungus || block is AbstractRoot;
                });
        }

        protected bool BelowEmpty(int x, int y, SandWorld world)
        {
            return _basket.All(offset => world[x + offset.X, y + offset.Y].Empty);
        }
    }
}

