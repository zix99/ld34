﻿using System.Collections.Generic;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Player : Sand
    {
        private const int VERT_SPEED = 4;

        private const int HORZ_SPEED = 2;

        private const float SPREAD = 0.05f;

        private const float MAX_DIST = 0.4f;

        private static readonly System.Random _random = new System.Random();

        public Player() : base(new Color32(225,15,15,255)) { _center = new Point2i(0, 0); }

        public float Horizontal;

        public float Vertical;

        private readonly HashSet<Point2i> _players = new HashSet<Point2i>();

        private Point2i _center;
        public Point2i Center
        {
            get{return _center;}
        }

        public override int Density
        {
            get
            {
                return 5;
            }
        }

        public int PlayerCount
        {
            get
            {
                lock (_players)
                    return _players.Count;
            }
        }

        public void ResetPlayers()
        {
            lock(_players)
            {
                _players.Clear();
                _center = new Point2i(0, 0);
            }
        }

        public IEnumerable<Point2i> LockGetPlayers()
        {
            lock(_players)
            {
                foreach (var pt in _players)
                    yield return pt;
            }
        }

        public Point2i GetCenter()
        {
            Point2i sum = Point2i.Zero;
            int count = 0;
            lock (_players)
            {
                foreach (var pt in this._players)
                {
                    sum.X += pt.X;
                    sum.Y += pt.Y;
                    ++count;
                }
            }
            if (count > 0)
                _center = new Point2i(sum.X / count, sum.Y / count);
            return _center;
        }

        public void WorldModify(int x, int y, byte newId, byte oldId)
        {
            lock (_players)
            {
                if (this.Id == newId)
                {
                    _players.Add(new Point2i(x, y));
                }
                else if (this.Id == oldId)
                {
                    _players.Remove(new Point2i(x, y));
                }
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            //Add to player list
            if(_center.X == 0 && _center.Y == 0)
            {
                lock(_players)
                    _players.Add(new Point2i(x, y));
                return true;
            }

            //Kill blocks too far away
            float maxAliveRange = Mathf.Max(PlayerCount * MAX_DIST, 8f);
            if (Mathf.Abs(_center.X - x) > maxAliveRange || Mathf.Abs(_center.Y - y) > maxAliveRange)
            {
                world[x, y] = SandMan.Get<PlayerDead>();
                return false;
            }

            //Kill blocks that could be touching deadly things
            if (world[x+1,y].Deadly
                || world[x-1,y].Deadly
                || world[x,y+1].Deadly
                || world[x,y-1].Deadly)
            {
                world[x, y] = SandMan.Get<PlayerDead>();
                return false;
            }

            //Calculate movement based on density and surroundings
            var dx = 0;
            var dy = 0;
            if (world[x, y - 1].Density < Density)
            {
                dy -= 1;
            }
            else if (world[x - 1, y - 1].Density < Density && world[x + 1, y - 1].Density < Density)
            {
                dy -= 1;
                if (_random.NextDouble() > 0.5)
                {
                    dx += 1;
                }
                else
                {
                    dx -= 1;
                }
            }
            else if (world[x - 1, y - 1].Density < Density)
            {
                dy -= 1;
                dx -= 1;
            }
            else if (world[x + 1, y - 1].Density < Density)
            {
                dy -= 1;
                dx += 1;
            }
            else if (Vertical > 0 && world[x + dx, y + dy + VERT_SPEED].Density < Density)
            {
                dy += VERT_SPEED;
            }

            if (Vertical > 0 && world[x + dx, y + dy + VERT_SPEED].Density < Density && world.Any(x, y, 3, s => s.Climbable))
            {
                dy += VERT_SPEED;
            }

            //Right movement for far-away blocks
            if (Horizontal > 0 || _center.X - x > PlayerCount * SPREAD)
            {
                if(world[x + dx + HORZ_SPEED, y + dy].Density < Density)
                {
                    dx += HORZ_SPEED;
                }
                else if (world[x + dx + HORZ_SPEED, y + dy + 1].Density < Density &&  Vertical >= 0)
                {
                    dx += HORZ_SPEED;
                    dy += 1;
                }
                else if (world[x + dx + HORZ_SPEED, y + dy + 2].Density < Density && Vertical >= 0)
                {
                    dx += HORZ_SPEED;
                    dy += 2;
                }
            }

            //Left movement for far-away blocks
            if (Horizontal < 0 || _center.X - x < -PlayerCount * SPREAD)
            {
                if (world[x + dx - HORZ_SPEED, y + dy].Density < Density)
                {
                    dx -= HORZ_SPEED;
                }
                else if (world[x + dx - HORZ_SPEED, y + dy + 1].Density < Density && Vertical >= 0)
                {
                    dx -= HORZ_SPEED;
                    dy += 1;
                }
                else if (world[x + dx - HORZ_SPEED, y + dy + 2].Density < Density && Vertical >= 0)
                {
                    dx -= HORZ_SPEED;
                    dy += 2;
                }
            }

            if(dx == 0 && dy == 0)
            {
                return true;
            }

            Swap(x, y, x + dx, y + dy, world);
            return true;
        }
    }
}
