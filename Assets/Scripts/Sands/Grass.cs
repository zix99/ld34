﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class Grass : AbstractPiling, IGrowable
    {
        public Grass()
            :base(new Color32(32, 150, 40, 255))
        {
            this.ColorVariance = 8;
        }
    }
}

