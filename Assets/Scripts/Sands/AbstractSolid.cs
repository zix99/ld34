﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class AbstractSolid : Sand
    {
        protected AbstractSolid(Color32 color)
            :base(color)
        {
            this.ColorVariance = 16;
        }

        public override int Density
        {
            get
            {
                return 100000;
            }
        }
    }
}

