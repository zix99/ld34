﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands
{
    public class AbstractPiling : Sand
    {
        private static readonly System.Random _random = new System.Random();

        protected AbstractPiling(Color32 color)
            :base(color)
        {
        }

        public override int Density
        {
            get
            {
                return 100;
            }
        }

        public override bool Simulate(int x, int y, SandWorld world)
        {
            if (world[x,y-1].Density < Density)
            {
                Swap(x, y, x, y - 1, world);
            }
            else if(world[x-1,y-1].Density < Density && world[x+1,y-1].Density < Density)
            {
                if (_random.NextDouble() > 0.5)
                {
                    //Go right
                    Swap(x, y, x + 1, y - 1, world);
                }
                else
                {
                    //Go left
                    Swap(x, y, x - 1, y - 1, world);
                }
            }
            else if(world[x-1,y-1].Density < Density)
            {
                Swap(x, y, x - 1, y - 1, world);
            }
            else if(world[x+1, y-1].Density < Density)
            {
                Swap(x, y, x + 1, y - 1, world);
            }
            return false;
        }
    }
}

