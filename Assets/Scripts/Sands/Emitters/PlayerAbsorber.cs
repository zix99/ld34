﻿using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Emitters
{
    public class PlayerAbsorber : AbstractAbsorber
    {
        public PlayerAbsorber()
            : base(new Color32(0, 10, 0, 255))
        {
        }

        protected override bool ShouldAbsorb(Sand type)
        {
            return type is Player;
        }
    }
}

