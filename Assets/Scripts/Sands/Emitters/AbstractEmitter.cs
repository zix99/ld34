﻿using System;
using System.Linq;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Emitters
{
    public abstract class AbstractEmitter : AbstractSolid
    {
        protected AbstractEmitter(Color32 color)
            :base(color)
        {
        }

        protected abstract Sand EmitType{get;}

        public sealed override bool Simulate(int x, int y, SandWorld world)
        {
            foreach(var offset in Point2i.AxisSides.Randomize())
            {
                if (world[x+offset.X,y+offset.Y].Empty)
                {
                    world[x + offset.X, y + offset.Y] = this.EmitType;
                    break;
                }
            }

            return true;
        }

        public override bool SimulateImmediately
        {
            get
            {
                return true;
            }
        }
    }
}

