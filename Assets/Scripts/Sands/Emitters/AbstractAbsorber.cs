﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Emitters
{
    public abstract class AbstractAbsorber : AbstractSolid
    {
        protected AbstractAbsorber(Color32 color)
            :base(color)
        {
        }

        protected abstract bool ShouldAbsorb(Sand type);

        public sealed override bool Simulate(int x, int y, SandWorld world)
        {
            foreach(var offset in Point2i.AxisSides)
            {
                if (ShouldAbsorb(world[x+offset.X,y+offset.Y]))
                {
                    world[x + offset.X, y + offset.Y] = SandMan.Empty;
                }
            }

            return true;
        }

        public override bool SimulateImmediately
        {
            get
            {
                return true;
            }
        }
    }
}

