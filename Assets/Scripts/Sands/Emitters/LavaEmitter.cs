﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Emitters
{
    public class LavaEmitter : AbstractEmitter
    {
        public LavaEmitter()
            :base(new Color32(5,0,0,255))
        {
        }

        protected override Sand EmitType
        {
            get
            {
                return SandMan.Get<Lava>();
            }
        }
    }
}

