﻿using System;
using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Emitters
{
    public class LiquidAbsorber : AbstractAbsorber
    {
        public LiquidAbsorber()
            :base(new Color32(0, 0, 5, 255))
        {
        }

        protected override bool ShouldAbsorb(Sand type)
        {
            return type is AbstractLiquid;
        }
    }
}

