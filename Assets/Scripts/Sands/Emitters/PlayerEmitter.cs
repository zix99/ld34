﻿using UnityEngine;

namespace AssemblyCSharp.Scripts.Sands.Emitters
{
    public class PlayerEmitter : AbstractEmitter
    {
        public PlayerEmitter()
            : base(new Color32(0,5,0,255))
        {
        }

        protected override Sand EmitType
        {
            get
            {
                return SandMan.Get<Player>();
            }
        }
    }
}
