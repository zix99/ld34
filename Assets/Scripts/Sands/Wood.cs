﻿namespace AssemblyCSharp.Scripts.Sands
{
    public class Wood : AbstractSolid
    {
        public Wood() : base(new UnityEngine.Color32(85, 45, 5, 255)) { }

        public override bool Flammable
        {
            get
            {
                return true;
            }
        }
    }
}
