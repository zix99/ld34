﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace AssemblyCSharp.Scripts
{
    public class SandWorld
    {
        public readonly int Width, Height;

        private readonly byte[] _buffer;

        private readonly HashSet<string> _sounds;

        public event Action<int, int, byte, byte> OnModify;

        public SandWorld(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            _buffer = new byte[width * height];
            _sounds = new HashSet<string>();
        }

        public Sand this [int x, int y]
        {
            get
            {
                if (x < 0 || y < 0 || x >= this.Width || y >= this.Height)
                    return SandMan.Bedrock;
                return SandMan.GetById( _buffer[y * Width + x] );
            }
            set
            {
                if (x < 0 || y < 0 || x >= this.Width || y >= this.Height)
                    return;

                byte curr = _buffer[y * Width + x];

                _buffer[y * Width + x] = value.Id;

                if (curr != value.Id)
                {
                    lock(_sounds)
                    {
                        if (value.SfxSimulate != null)
                            _sounds.Add(value.SfxSimulate);
                    }

                    if (OnModify != null)
                    {
                        OnModify(x, y, value.Id, curr);
                    }                
                }
                    
            }
        }

        public List<string> GetSounds()
        {
            lock(_sounds)
            {
                var s = _sounds.ToList();
                _sounds.Clear();
                return s;
            }
        }

        public void Clear()
        {
            Clear(SandMan.Empty);
        }

        public void Clear(Sand sand)
        {
            Debug.Log("Clear with " + sand);
            for (int i=0; i<_buffer.Length; ++i)
            {
                _buffer[i] = sand.Id;
            }
        }

        #region Helpers

        /// <summary>
        /// Return true if all block surround the coordinate with size qualifies predicate
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="sz">Size.</param>
        /// <param name="predicate">Predicate.</param>
        public bool Surrounds(int x, int y, int sz, Func<Sand, bool> predicate)
        {
            for (int s = x - sz; s<=x + sz; ++s)
            {
                for (int t = y - sz; t<=y+sz; ++t)
                {
                    if (s == x && t == y)
                        continue;

                    if (!predicate(this[s, t]))
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns true if any block in the size around x,y qualifies predicate
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="sz">Size.</param>
        /// <param name="predicate">Predicate.</param>
        public bool Any(int x, int y, int sz, Func<Sand, bool> predicate)
        {
            for (int s = x - sz; s<=x + sz; ++s)
            {
                for (int t = y - sz; t<=y+sz; ++t)
                {
                    if (predicate(this[s, t]))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Returns true if any block except the center one qualifies predicate
        /// </summary>
        /// <returns><c>true</c>, if other was anyed, <c>false</c> otherwise.</returns>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="sz">Size.</param>
        /// <param name="predicate">Predicate.</param>
        public bool AnyOther(int x, int y, int sz, Func<Sand, bool> predicate)
        {
            for (int s = x - sz; s<=x + sz; ++s)
            {
                for (int t = y - sz; t<=y+sz; ++t)
                {
                    if (s == x && t == y)
                        continue;

                    if (predicate(this[s, t]))
                        return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Count the number of blocks that match the predicate.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="sz">Size.</param>
        /// <param name="predicate">Predicate.</param>
        /// <returns>The number of blocks that match the predicate</returns>
        public int Count(int x, int y, int sz, Func<Sand, bool> predicate)
        {
            int c = 0;
            for (int s = x - sz; s <= x + sz; ++s)
            {
                for (int t = y - sz; t <= y + sz; ++t)
                {
                    if (predicate(this[s, t]))
                        c++;
                }
            }
            return c;
        }

        /// <summary>
        /// Count the number of blocks except the center one that match the predicate.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="sz">Size.</param>
        /// <param name="predicate">Predicate.</param>
        /// <returns>The number of blocks except the center one that match the predicate</returns>
        public int CountOther(int x, int y, int sz, Func<Sand, bool> predicate)
        {
            int c = 0;
            for (int s = x - sz; s <= x + sz; ++s)
            {
                for (int t = y - sz; t <= y + sz; ++t)
                {
                    if (s == x && t == y)
                        continue;

                    if (predicate(this[s, t]))
                        c++;
                }
            }
            return c;
        }

        /// <summary>
        /// Get the closest sand 
        /// </summary>
        /// <returns>The where.</returns>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="z">The z coordinate.</param>
        /// <param name="predicate">Predicate.</param>
        public Sand ClosestWhere(int x, int y, int sz, Func<Sand, bool> predicate)
        {
            for (int d=1; d<sz; ++d)
            {
                for (int i=0; i<Point2i.AllSides.Length; ++i)
                {
                    var pt = new Point2i(x + Point2i.AllSides[i].X * d, y + Point2i.AllSides[i].Y * d);
                    if (predicate(this[pt.X, pt.Y]))
                        return this[pt.X, pt.Y];
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the point of the closest satisfying a predicate
        /// </summary>
        /// <returns>The where.</returns>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="sz">Size.</param>
        /// <param name="predicate">Predicate.</param>
        public Point2i? FindWhere(int x, int y, int sz, Func<Sand, bool> predicate)
        {
            for (int d=1; d<sz; ++d)
            {
                for (int i=0; i<Point2i.AllSides.Length; ++i)
                {
                    var pt = new Point2i(x + Point2i.AllSides[i].X * d, y + Point2i.AllSides[i].Y * d);
                    if (predicate(this[pt.X, pt.Y]))
                        return pt;
                }
            }
            return null;
        }

        #endregion
    }
}

