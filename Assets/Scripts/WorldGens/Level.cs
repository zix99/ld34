﻿using System;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace AssemblyCSharp.Scripts.WorldGens
{
    public class Level
    {
        private readonly string _path;

        public readonly int LevelNumber;

        public Level(int levelNum)
        {
            this.LevelNumber = levelNum;
            _path = string.Format("Levels/{0:D2}/", levelNum);
        }

        public void LoadWorld(SandWorld toWorld)
        {
            var tex = Resources.Load(_path + "world") as Texture2D;

            toWorld.Clear(SandMan.Bedrock);

            if (tex != null)
            {
                for (int y = 0; y < tex.height; ++y)
                {
                    for (int x = 0; x < tex.width; ++x)
                    {
                        var color = (Color32)tex.GetPixel(x, y);
                        toWorld[x, y] = SandMan.GetByNearestColor(color);
                    }
                }
            }
            else
            {
                Debug.LogWarning("Unable to load world for " + _path);
            }
        }

        public Texture2D LoadBackground()
        {
            return Resources.Load(_path + "background") as Texture2D;
        }

        public IEnumerable<KeyValuePair<string,int>> GetItemAndCount()
        {
            var res = Resources.Load(_path + "items") as TextAsset;

            if (res != null && !string.IsNullOrEmpty(res.text))
            {
                using (var reader = new StringReader(res.text))
                {
                    string line;
                    while((line = reader.ReadLine()) != null)
                    {
                        if (!string.IsNullOrEmpty(line) && !line.StartsWith("#"))
                        {
                            string[] vals = line.Split(',');
                            yield return new KeyValuePair<string, int>(vals[0], int.Parse(vals[1]));
                        }
                    }
                }
            }
        }
    }
}

