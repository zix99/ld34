﻿using System;

namespace AssemblyCSharp.Scripts
{
    public static class Events
    {
        public static event Action LevelCompleteEvent;

        public static void LevelCompeted()
        {
            if(LevelCompleteEvent != null)
            {
                LevelCompleteEvent();
            }
        }
    }
}
