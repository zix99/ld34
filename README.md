# Sand Storm

For, Ludum Dare 34

## Working with

You'll need Unity 5.3.0 or higher.

## Playing

Keys: WSAD
Reset: R
Mouse Click
1-9 Select

## License

Copyright (C) 2015 Chris LaPointe & Devin Stiert

Attribution 4.0 International (CC BY 4.0)

https://creativecommons.org/licenses/by/4.0/